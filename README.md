# Move to https://gitlab.com/mzdonline/cmu-opkg

Move to https://gitlab.com/mzdonline/cmu-opkg/tree/master/ports

## README

Command line collection for CMU

|Name|version|src|Example|Description|
|--|--|--|--|--|
|[evtest](/bin)||[Links](git://anongit.freedesktop.org/evtest)|./evtest /dev/input/event0|displays information on the input device specified on the command line, including all the events supported by the device. It then monitors the device and displays all the events layer events generated.|
|[websocketd](/bin)| 0.2.12|[websocketd-0.2.12-linux_arm.zip](https://github.com/joewalnes/websocketd)|websocketd --port=9999 sh|Turn any program that uses STDIN/STDOUT into a WebSocket server. Like inetd, but for WebSockets.|
|[node](/node)|v0.12.7|[links](https://github.com/nodejs/node/tree/v0.12.7-release)|node -v|JavaScript runtime|
|[rtlsdr](/rtlsdr)||http://osmocom.org/projects/sdr/wiki/rtl-sdr|./rtl_test|RTL2832U can be used as a cheap SDR|
|[wget](/wget)|1.19|[links](http://ftp.gnu.org/gnu/wget/wget-1.19.tar.xz)|./wget --no-check-certificate https://gitlab.com/mzdonline/cmu-opkg/raw/master/arm/packages/base/Packages.gz| Wget support HTTPS |