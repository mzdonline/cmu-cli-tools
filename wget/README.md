# Feature
- support SSL

# Install

* on PC
```
scp wget user@192.168.42.1:/data_persist/dev/bin
scp libuuid.so.1.3.0 user@192.168.42.1:/data_persist/dev/lib
```

* On CMU 
```
cd /data_persist/dev/lib
ln -s libuuid.so.1.3.0 libuuid.so.1
alias wget="wget --no-check-certificate"
echo "check_certificate = off" >> ${HOME}/.wgetrc
echo "check_certificate = off" >> /tmp/user/.wgetrc
chown user /tmp/user/.wgetrc
mount -o rw,remount /
cd /usr/bin
ln -sf /data_persist/dev/bin/wget 
wget --version
```

# Build
```bash
cd /opt
git clone https://github.com/lmagder/m3-toolchain
```

edit ~/.bashrc. add compiler path

```bash
PATH="/opt/m3-toolchain/bin:$PATH"
```

# Make wget for cmu

```bash
M3TOOLCHAIN=/opt/mazda/m3-toolchain
CC=$(M3TOOLCHAIN)/bin/arm-cortexa9_neon-linux-gnueabi-gcc
CXX=$(M3TOOLCHAIN)/bin/arm-cortexa9_neon-linux-gnueabi-g++
LD=$(M3TOOLCHAIN)/bin/arm-cortexa9_neon-linux-gnueabi-ld
SYSROOT=$(M3TOOLCHAIN)/arm-cortexa9_neon-linux-gnueabi/sysroot

INCLUDES=$(shell ./pkg-config-wrapper $(SYSROOT) --cflags dbus-1 libusb-1.0  libcrypto openssl glib-2.0 dbus-c++-1 dbus-c++-glib-1 libudev)

CFLAGS=-Os -s -rdynamic -pthread --sysroot=$(SYSROOT) -DCMU=1 -D__STDC_FORMAT_MACROS -march=armv7-a -mtune=cortex-a9 -mfpu=neon

./configure --host=arm-cortexa9_neon-linux-gnueabi --with-openssl --with-ssl=openssl
make
```