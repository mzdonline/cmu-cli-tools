#!/bin/sh -e

cd ${HOME}
if ! [ -e ${HOME}/m3-toolchain ]
 then
  git clone https://github.com/lmagder/m3-toolchain
fi
M3TOOLCHAIN=${HOME}/m3-toolchain
CC=${M3TOOLCHAIN}/bin/arm-cortexa9_neon-linux-gnueabi-gcc
CXX=${M3TOOLCHAIN}/bin/arm-cortexa9_neon-linux-gnueabi-g++
LD=${M3TOOLCHAIN}/bin/arm-cortexa9_neon-linux-gnueabi-ld
SYSROOT=${M3TOOLCHAIN}/arm-cortexa9_neon-linux-gnueabi/sysroot
chmod +x pkg-config-wrapper
INCLUDES=`./pkg-config-wrapper ${SYSROOT} --cflags dbus-1 libusb-1.0  libcrypto openssl glib-2.0 dbus-c++-1 dbus-c++-glib-1 libudev`

CFLAGS="-Os -rdynamic -pthread --sysroot=${SYSROOT} -DCMU=1 -D__STDC_FORMAT_MACROS -march=armv7-a -mtune=cortex-a9 -mfpu=neon"

wget http://ftp.gnu.org/gnu/wget/wget-1.19.tar.xz
tar xf wget-1.19.tar.xz
cd wget-1.19
./configure --host=arm-cortexa9_neon-linux-gnueabi --with-openssl --with-ssl=openssl
make
