NODE_VERSION=0.12.9
M3=${HOME}/m3-toolchain
export PKG_CONFIG_PATH=${M3}/arm-cortexa9_neon-linux-gnueabi/sysroot/usr/lib/pkgconfig
export CC=${M3}/bin/arm-cortexa9_neon-linux-gnueabi-gcc
export CXX=${M3}/bin/arm-cortexa9_neon-linux-gnueabi-g++
export AR=${M3}/bin/arm-cortexa9_neon-linux-gnueabi-ar
export RANLIB=${M3}/bin/arm-cortexa9_neon-linux-gnueabi-ranlib
export LINK="${CXX}"
export CCFLAGS="-Wl -march=armv7-a --build-id=none"
export CXXFLAGS="-mtune=cortex-a8 -mfpu=neon -mfloat-abi=softfp -mthumb-interwork"
export NODE_SOURCE_PACKAGE=node-v${NODE_VERSION}

sudo apt-get -y update
sudo apt-get -y install git

if ! [ -e m3-toolchain ];
then
 git clone https://github.com/lmagder/m3-toolchain
fi

if ! [ -e ${NODE_SOURCE_PACKAGE}.tar.gz ];
 then
  wget https://nodejs.org/dist/v${NODE_VERSION}/${NODE_SOURCE_PACKAGE}.tar.gz
fi
tar xf ${NODE_SOURCE_PACKAGE}.tar.gz
cd ${NODE_SOURCE_PACKAGE}
./configure --without-snapshot --dest-cpu=arm --dest-os=linux --fully-static   --shared-openssl --tag=CASDK-NODE
make
