# Build nodejs [![build status](https://gitlab.com/mzdonline/cmu-cli-tools/badges/master/build.svg)](https://gitlab.com/mzdonline/cmu-cli-tools/commits/master)

```
git clone --recursive https://gitlab.com/mzdonline/cmu-cli-tools
cp cmu-cli-tools/nodejs/nodejs.sh cmu-cli-tools/nodejs/v0.12.7-release
cd cmu-cli-tools/nodejs/v0.12.7-release
./nodejs.sh
```
